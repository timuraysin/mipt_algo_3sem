#include <iostream>
#include <vector>
#include <algorithm>
#include <set>

struct Point {
    double x;
    double y;

    Point(double _x, double _y) {
        x = _x;
        y = _y;
    }

    Point() : Point(0, 0) {}

    Point operator+(const Point& p) const {
        return Point(x + p.x, y + p.y);
    }

    Point operator-(const Point& p) const {
        return Point(x - p.x, y - p.y);
    }

    bool operator<(const Point& p) const {
        return (y < p.y) || (y == p.y && x < p.x);
    }

    bool operator==(const Point& p) const {
        return (y == p.y && p.x == x);
    }
};

class Segment {
public:
    Point start;
    Point end;
    int ind;

    Segment(Point p1, Point p2, int i) {
        if (p1.x > p2.x) {
            start = p2;
            end = p1;
        } else {
            start = p1;
            end = p2;
        }
        ind = i;
    }

    double GetY(double x) const {
        if (std::abs(start.x - end.x) < EPS) {
            return start.y;
        }
        return start.y + (end.y - start.y) * (x - start.x) / (end.x - start.x);
    }

    bool operator<(const Segment& s) const {
        double x = std::max(start.x, s.start.x);
        return GetY(x) < s.GetY(x) - EPS;
    }

    bool operator==(const Segment& s) const {
        return (std::abs(start.x - s.start.x) < EPS && std::abs(start.y - s.start.y) < EPS &&
                std::abs(end.x - s.end.x) < EPS && std::abs(end.y == s.end.y) < EPS);
    }

    bool Between(Point a, Point b, Point c) const {
        return (a.x <= c.x + EPS) && (c.x <= b.x + EPS) && (std::min(a.y, b.y) <= c.y + EPS) && (c.y <= std::max(a.y, b.y) + EPS);
    }

    bool Coinciding(const Segment& s) const {
        if (std::abs(GetY(s.start.x) - s.start.y) < EPS) {
            return true;
        }

        return false;
    }

    bool DoIntersect(const Segment& s) const {
        bool no_collinear = CountDeterminant(start.x - end.x, start.y - end.y, s.start.x - s.end.x,
                                              s.start.y - s.end.y) != 0;
        if (!no_collinear) {
            return Coinciding(s);
        }

        Point intersection = GetIntersection(s);

        return Between(start, end, intersection) && Between(s.start, s.end, intersection);
    }

    Point GetIntersection(const Segment& s) const {
        double d = CountDeterminant(
                CountDeterminant(start.x, 1, end.x, 1),
                CountDeterminant(start.y, 1, end.y, 1),
                CountDeterminant(s.start.x, 1, s.end.x, 1),
                CountDeterminant(s.start.y, 1, s.end.y, 1));
        double x = CountDeterminant(
                CountDeterminant(start.x, start.y, end.x, end.y),
                CountDeterminant(start.x, 1, end.x, 1),
                CountDeterminant(s.start.x, s.start.y, s.end.x, s.end.y),
                CountDeterminant(s.start.x, 1, s.end.x, 1));
        x /= d;

        double y = CountDeterminant(
                CountDeterminant(start.x, start.y, end.x, end.y),
                CountDeterminant(start.y, 1, end.y, 1),
                CountDeterminant(s.start.x, s.start.y, s.end.x, s.end.y),
                CountDeterminant(s.start.y, 1, s.end.y, 1));
        y /= d;

        return Point(x, y);
    }
private:
    double CountDeterminant(double x1, double y1, double x2, double y2) const {
        return x1 * y2 - x2 * y1;
    }

    static double EPS;
};

double Segment::EPS = 1E-9;

std::set<Segment>::iterator Prev(const std::set<Segment>& s, std::set<Segment>::iterator it) {
    return it == s.begin() ? s.end() : --it;
}

std::set<Segment>::iterator Next(const std::set<Segment>& s, std::set<Segment>::iterator it) {
    return ++it;
}

enum class EventType {
    OPEN_SEGMENT,
    CLOSE_SEGMENT
};

class Event {
public:
    double x;
    EventType event_type;
    int ind;

    Event(double _x, EventType _type, int _ind) {
        x = _x;
        event_type = _type;
        ind = _ind;
    }

    bool operator<(const Event& e) const {
        if (std::abs(x - e.x) > EPS) {
            return x < e.x;
        }
        if (event_type == EventType::OPEN_SEGMENT && e.event_type == EventType::CLOSE_SEGMENT) {
            return true;
        }
        else if (event_type == EventType::CLOSE_SEGMENT && e.event_type == EventType::OPEN_SEGMENT) {
            return false;
        }
        return false;
    }

private:
    static double EPS;
};

double Event::EPS = 1E-9;

class Solution {
public:
    Solution(std::vector<Segment>&& _segments) {
        segments = _segments;
    }

    std::pair<int, int> Solve() {
        std::vector<Event> events;
        for (size_t i = 0; i < segments.size(); ++i) {
            events.emplace_back(segments[i].start.x, EventType::OPEN_SEGMENT, i);
            events.emplace_back(segments[i].end.x, EventType::CLOSE_SEGMENT, i);
        }

        sort(events.begin(), events.end());
        std::set<Segment> active_segments;
        std::vector<std::set<Segment>::iterator> segment_it(segments.size());

        for (size_t i = 0; i < events.size(); ++i) {
            int ind = events[i].ind;
            if (events[i].event_type == EventType::OPEN_SEGMENT) {
                std::set<Segment>::iterator nxt = active_segments.lower_bound(segments[ind]);
                std::set<Segment>::iterator prv = Prev(active_segments, nxt);

                if (nxt != active_segments.end() && segments[ind].DoIntersect(*nxt)) {
                    ReturnSegments(nxt->ind, ind);
                }

                if (prv != active_segments.end() && segments[ind].DoIntersect(*prv)) {
                    ReturnSegments(prv->ind, ind);
                }

                segment_it[ind] = active_segments.insert(nxt, segments[ind]);
            } else {
                std::set<Segment>::iterator nxt = Next(active_segments, segment_it[ind]);
                std::set<Segment>::iterator prv = Prev(active_segments, segment_it[ind]);

                if (nxt != active_segments.end() && prv != active_segments.end() && nxt->DoIntersect(*prv)) {
                    ReturnSegments(prv->ind, nxt->ind);
                }
                active_segments.erase(segment_it[ind]);
            }
        }

        return {-1, -1};
    }

private:
    std::vector<Segment> segments;

    std::pair<int, int> ReturnSegments(int n1, int n2) {
        if (n1 > n2) {
            std::swap(n1, n2);
        }
        return {n1 + 1, n2 + 1};
    }
};

int main() {
    int number_of_segments;
    std::cin >> number_of_segments;
    std::vector<Segment> segments;

    for (int i = 0; i < number_of_segments; ++i) {
        Point p1, p2;
        std::cin >> p1.x >> p1.y >> p2.x >> p2.y;

        segments.push_back(Segment(p1, p2, i));
    }

    Solution solution(std::move(segments));
    std::pair<int, int> res = solution.Solve();

    if (res.first != -1) {
        std::cout << "YES" << std::endl;
        std::cout << res.first << " " << res.second << std::endl;
    } else {
        std::cout << "NO" << std::endl;
    }

    return 0;
}