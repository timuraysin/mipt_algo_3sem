#include <iostream>
#include <string>
#include <vector>
#include <deque>

// Can return vector because of Return-Value optimization.
std::vector<int> ComputeZFuncForPattern(const std::string& pattern) {
    std::vector<int> z_func = std::vector<int>(pattern.size(), 0);

    // The start and end of the largest z-box with the largest right border, respectively.
    int z_block_start = 0;
    int z_block_end = 0;

    z_func[0] = 0;
    for (int i = 1; i < pattern.size(); ++i) {
        if (i <= z_block_end) {
            z_func[i] = std::min(z_block_end - i, z_func[i - z_block_start]);
        }

        while (i + z_func[i] < pattern.size() && pattern[z_func[i]] == pattern[i + z_func[i]]) {
            z_func[i]++;
        }

        if (i + z_func[i] - 1 > z_block_end) {
            z_block_start = i;
            z_block_end = i + z_func[i] - 1;
        }
    }

    return z_func;
}

std::vector<int> FindIndecesOfSubstrings(const std::string& pattern, const std::string& input) {
    // Finding substrings uses the string z-function {pattern}.
    std::vector<int> z_func = ComputeZFuncForPattern(pattern);

    int i = 0;
    std::deque<char> last_P_characters; // Last inputted |p.size()| characters.

    while (i < pattern.size() - 1 && i < input.size()) {
        last_P_characters.push_back(input[i]);
        i++;
    }

    // The start and end of the largest z-box with the largest right border, respectively.
    int z_block_start = 0;
    int z_block_end = 0;

    int offset = i;
    i = 0;

    std::vector<int> result;

    while (i + offset < input.size()) {
        last_P_characters.push_back(input[i + offset]);

        int z_value = 0;
        if (i <= z_block_end) {
            z_value = std::min(z_block_end - i, z_func[i - z_block_start]);
        }

        while (z_value < pattern.size() && pattern[z_value] == last_P_characters[z_value]) {
            z_value++;
        }

        if (z_value == pattern.size()) {
            result.push_back(i);
        }

        if (i + z_value - 1 > z_block_end) {
            z_block_start = i;
            z_block_end = i + z_value - 1;
        }

        i++;
        last_P_characters.pop_front();
    }

    return result;
}

int main() {
    // Optimize std::cin
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::string pattern;
    std::cin >> pattern;

    std::string input;
    std::cin >> input;

    std::vector<int> res = FindIndecesOfSubstrings(pattern, input);
    for (auto x : res) {
        std::cout << x << " ";
    }

    std::cout << std::endl;

    return 0;
}