#include <iostream>
#include <vector>
#include <iomanip>
#include <stack>
#include <cmath>
#include <algorithm>

enum class Orient {
    kOrientationLeft,
    kOrientationRight,
    kZero
};

struct Point {
    double x;
    double y;

    Point(double _x, double _y) {
        x = _x;
        y = _y;
    }

    Point() : Point(0, 0) {}

    double GetRelativeLength(const Point& p0) const {
        return (x - p0.x) * (x - p0.x) + (y - p0.y) * (y - p0.y);
    }

    bool operator<(const Point& other_point) {
        if (y < other_point.y) {
            return true;
        } else if (y > other_point.y) {
            return false;
        } else return (x < other_point.x);
    }

    static bool ComparePoints(const Point& p1, const Point& p2);

    static Point p0;
};

Orient Orientation(const Point& p0, const Point& p1, const Point& p2) {
    double x1 = p1.x - p0.x;
    double y1 = p1.y - p0.y;
    double x2 = p2.x - p1.x;
    double y2 = p2.y - p1.y;

    if (x1 * y2 - y1 * x2 > 0) {
        return Orient::kOrientationLeft;
    } else if (x1 * y2 - y1 * x2 < 0) {
        return Orient::kOrientationRight;
    } else {
        return Orient::kZero;
    }
}

bool Point::ComparePoints(const Point &p1, const Point &p2) {
    if (Orientation(p0, p1, p2) == Orient::kOrientationLeft) {
        return true;
    } else if (Orientation(p0, p1, p2) == Orient::kZero) {
        return p1.GetRelativeLength(p0) < p2.GetRelativeLength(p0);
    }

    return false;
}

Point Point::p0;

class Solution {
public:
    Solution(int _number_of_points, std::vector<Point>&& _points) {
        number_of_points = _number_of_points;
        points = _points;

        Point lowest = *(std::min_element(points.begin(), points.end()));

        std::sort(points.begin() + 1, points.end(), Point::ComparePoints);
    }

    double solve() {
        std::vector<Point> hull;
        hull.emplace_back(points[0]);
        hull.emplace_back(points[1]);

        for (int i = 2; i < number_of_points; i++) {
            while (hull.size() >= 2 &&
                    ((Orientation(NextToTop(hull), hull[hull.size() - 1], points[i]) == Orient::kOrientationRight) ||
                     Orientation(NextToTop(hull), hull[hull.size() - 1], points[i]) == Orient::kZero)) {
                hull.pop_back();
            }

            hull.emplace_back(points[i]);
        }

        double hull_length = 0;

        for (int i = 0; i < hull.size() - 1; ++i) {
            hull_length += sqrt(hull[i].GetRelativeLength(hull[i + 1]));
        }

        double x = hull[hull.size() - 1].x - hull[0].x;
        double y = hull[hull.size() - 1].y - hull[0].y;
        hull_length += sqrt(x * x + y * y);

        return hull_length;
    }

private:
    int number_of_points;
    std::vector<Point> points;

    Point NextToTop(std::vector<Point> &S) {
        return S[S.size()-2];
    }
};

int main() {
    int number_of_points = 0;
    std::cin >> number_of_points;

    std::vector<Point> points;
    for (int i = 0; i < number_of_points; ++i) {
        Point a;
        std::cin >> a.x >> a.y;

        points.push_back(a);
    }

    Solution solution(number_of_points, std::move(points));
    double hull_length = solution.solve();

    std::cout << std::fixed << std::setprecision(12) << hull_length << std::endl;

    return 0;
}