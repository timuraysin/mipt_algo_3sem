#include <iostream>
#include <vector>

struct Point {
    double x;
    double y;

    Point(double _x, double _y) {
        x = _x;
        y = _y;
    }

    Point() : Point(0, 0) {}
};

class Line {
public:
    Line(const Point& p1, const Point& p2) {
        first = p1;
        second = p2;
    }

    Line() {
        first = Point(0, 0);
        second = Point(0, 0);
    }

    bool DoIntersect(const Line& l) {
        double d = CountDeterminant(CountDeterminant(first.x, 1, second.x, 1),
                                    CountDeterminant(first.y, 1, second.y, 1),
                                    CountDeterminant(l.first.x, 1, l.second.x, 1),
                                    CountDeterminant(l.first.y, 1, l.second.y, 1));

        if (d == 0) {
            return false;
        }

        double x = CountDeterminant(CountDeterminant(first.x, first.y, second.x, second.y),
                                    CountDeterminant(first.x, 1, second.x, 1),
                                    CountDeterminant(l.first.x, l.first.y, l.second.x, l.second.y),
                                    CountDeterminant(l.first.x, 1, l.second.x, 1));
        x /= d;

        double y = CountDeterminant(CountDeterminant(first.x, first.y, second.x, second.y),
                                    CountDeterminant(first.y, 1, second.y, 1),
                                    CountDeterminant(l.first.x, l.first.y, l.second.x, l.second.y),
                                    CountDeterminant(l.first.y, 1, l.second.y, 1));
        y /= d;

        if (x >= std::min(first.x, second.x) && x <= std::max(first.x, second.x)
            && y <= std::max(first.y, second.y) && y >= std::min(first.y, second.y) &&
            x >= std::min(l.first.x, l.second.x) && x <= std::max(l.first.x, l.second.x) &&
            y >= std::min(l.first.y, l.second.y) && y <= std::max(l.first.y, l.second.y)) {
            return true;
        } else {
            return false;
        }
    }

private:
    Point first;
    Point second;

    double CountDeterminant(double x1, double y1, double x2, double y2) {
        return x1 * y2 - x2 * y1;
    }
};

class Solution {
public:
    Solution(int _number_of_lines, Point _a, Point _b) {
        A = _a;
        B = _b;
        number_of_lines = _number_of_lines;

        road = Line(A, B);
    }

    int Solve(std::vector<Point>&& points) {
        int ans = 0;

        for (int i = 0; i < number_of_lines; i++) {
            Point C = points[i];
            Point D = points[i + 1];

            Line bridge(C, D);

            if (road.DoIntersect(bridge)) {
                ans++;
            }
        }

        return ans;
    }

private:
    int number_of_lines;
    Point A, B;
    Line road;
};

int main() {
    int number_of_lines;
    std::cin >> number_of_lines;

    Point A, B;
    std::cin >> A.x >> A.y >> B.x >> B.y;

    Solution sol(number_of_lines, A, B);

    std::vector<Point> points;
    for (int i = 0; i < number_of_lines; ++i) {
        Point C, D;

        std::cin >> C.x >> C.y >> D.x >> D.y;

        points.push_back(C);
        points.push_back(D);
    }

    int count_intersections = sol.Solve(std::move(points));

    std::cout << count_intersections << std::endl;

    return 0;
}
