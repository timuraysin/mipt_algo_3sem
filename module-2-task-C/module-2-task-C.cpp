#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include <unordered_map>
#include <set>
#include <list>
#include <unordered_set>

class Point {
public:
    int64_t x;
    int64_t y;
    int64_t z;

    Point() : x(0), y(0), z(0) {}

    Point(int64_t _x, int64_t _y, int64_t _z) : x(_x), y(_y), z(_z) {}

    Point& operator=(const Point& p) {
        x = p.x;
        y = p.y;
        z = p.z;

        return *this;
    }
};

struct PointKey {
    std::size_t operator() (const Point& p) const {
        return std::hash<std::string>{}(std::to_string(p.x) + std::to_string(p.y) + std::to_string(p.z));
    }
};

bool operator<(const Point& p1, const Point& p2) {
    return (p1.z < p2.z) || (p1.z == p2.z && p1.y < p2.y) || (p1.z == p2.z && p1.y == p2.y && p1.x < p2.x);
}

bool operator==(const Point& p1, const Point& p2) {
    return (p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z);
}

bool operator!=(const Point& p1, const Point& p2) {
    return !(p1 == p2);
}

class Vector {
public:
    int64_t x;
    int64_t y;
    int64_t z;

    Vector(int64_t _x, int64_t _y, int64_t _z) : x(_x), y(_y), z(_z) {}

    int64_t ScalarProduct(Vector v) {
        return x * v.x + y * v.y + z * v.z;
    }

    Vector operator*(double k) {
        return Vector(x * k, y * k, z * k);
    }

    Vector operator/(double k) {
        return Vector(x / k, y / k, z / k);
    }

    Vector VectorProduct(Vector v) {
        int64_t nx = y * v.z - z * v.y;
        int64_t ny = z * v.x - x * v.z;
        int64_t nz = x * v.y - y * v.x;

        Vector prod(nx, ny, nz);
        prod = prod / prod.Module();

        return prod;
    }

    double Module() {
        return std::sqrt(x * x + y * y + z * z);
    }
};

class Face {
public:
    bool visible = false;
    int64_t a;
    int64_t b;
    int64_t c;
    int64_t d;

    std::vector<Point> vtx;

    Face(const Point& p1, const Point& p2, const Point& p3) {
        vtx.push_back(p1);
        vtx.push_back(p2);
        vtx.push_back(p3);

        a = (p2.y - p1.y) * (p3.z - p1.z) - (p3.y - p1.y) * (p2.z - p1.z);
        b = (p3.x - p1.x) * (p2.z - p1.z) - (p3.z - p1.z) * (p2.x - p1.x);
        c = (p2.x - p1.x) * (p3.y - p1.y) - (p3.x - p1.x) * (p2.y - p1.y);
        d = -a * p1.x - b * p1.y - c * p1.z;
    }

    static int64_t TetrahedronVolume(const Face& f, Point p) {
        int64_t vol;
        int64_t ax, ay, az, bx, by, bz, cx, cy, cz;
        
        ax = f.vtx[0].x - p.x;
        ay = f.vtx[0].y - p.y;
        az = f.vtx[0].z - p.z;
        bx = f.vtx[1].x - p.x;
        by = f.vtx[1].y - p.y;
        bz = f.vtx[1].z - p.z;
        cx = f.vtx[2].x - p.x;
        cy = f.vtx[2].y - p.y;
        cz = f.vtx[2].z - p.z;

        vol = ax * (by * cz - bz * cy) + ay * (bz * cx - bx * cz) + az * (bx * cy - by * cx);
        if(vol == 0) {
            return 0;
        }
        return vol < 0 ? -1 : 1;
    }
};

bool operator==(const Face& f1, const Face& f2) {
    for (int i = 0; i < 3; ++i) {
        bool flag = true;
        for (int j = 0; j < 3; ++j) {
            if (f1.vtx[(i + j) % 3] != f2.vtx[j]) {
                flag = false;
            }
        }

        if (flag) {
            return true;
        }
    }

    return false;
}


class Edge : public Vector {
public:
    Point p0;
    Point p1;

    Face* face1;
    Face* face2;

    bool remove;

    Edge(Point _p0, Point _p1) : Vector(_p1.x - _p0.x, _p1.y - _p0.y, _p1.z - _p0.z) {
        remove = false;
        face1 = nullptr;
        face2 = nullptr;
        p0 = _p0;
        p1 = _p1;
    }

    void AddFace(Face* f) {
        if (face1 == nullptr) {
            face1 = f;
        } else if (face2 == nullptr) {
            face2 = f;
        }
    }

    int CountFaces() {
        if (face1 != nullptr && face2 != nullptr) {
            return 2;
        } else if (face1 != nullptr || face2 != nullptr) {
            return 1;
        } else return 0;
    }

    void RemoveFace(Face* f) {
        if (face1 == f) {
            face1 = nullptr;
        } else if (face2 == f) {
            face2 = nullptr;
        }
    }

    size_t GetKey() {
        PointKey h;
        return h(p0) ^ h(p1);
    }
};

bool operator<(const Edge& e1, const Edge& e2) {
    return (e1.z < e2.z) || (e1.z == e2.z && e1.y < e2.y) || (e1.z == e2.z && e1.y == e2.y && e1.x < e2.x);
}

bool operator==(const Edge& e1, const Edge& e2) {
    return (e1.p0 == e2.p0 && e1.p1 == e2.p1) || (e1.p0 == e2.p1 && e1.p1 == e2.p0);
}

bool operator!=(const Edge& e1, const Edge& e2) {
    return !(e1 == e2);
}

class Solution {
public:
    Solution(std::vector<Point>&& _points) : points(_points) {
        BuildHull();
    }

    double Solve(int query_number, std::vector<Point>&& worms) {
        Point worm;
        for (int i = 0; i < query_number; ++i) {
            worm = worms[i];

            double len = -1;

            for (auto iter = hull.begin(); iter != hull.end(); ++iter) {
                double A = iter->a;
                double B = iter->b;
                double C = iter->c;
                double D = iter->d;

                double val1 = std::abs(A * worm.x + B * worm.y + C * worm.z + D);
                double val2 = std::sqrt(A * A + B * B + C * C);

                double path = val1 / val2;

                if (len == -1 || len > path) {
                    len = path;
                }
            }

            return len;
        }
    }

private:
    std::vector<Point> points;
    std::list<Face> hull;
    std::list<Edge> edges;
    std::unordered_map<size_t, Edge*> edge_to_faces;
    std::set<Point> processed;

    // using Incremental algorithm
    void BuildHull() {
        Point p1 = points[0];
        Point p2 = p1;

        for (int i = 1; i < points.size(); ++i) {
            if (points[i] != p1) {
                p2 = points[i];
                break;
            }
        }

        Point p3 = p2;
        for (int i = 1; i < points.size(); ++i) {
            if (points[i] != p1 && points[i] != p2) {
                p3 = points[i];
                break;
            }
        }

        Point p4 = p3;

        for (int i = 1; i < points.size(); ++i) {
            if (points[i] != p1 && points[i] != p2 && points[i] != p3) {
                p4 = points[i];
                break;
            }
        }

        processed.insert(p1);
        processed.insert(p2);
        processed.insert(p3);
        processed.insert(p4);

        AddFace(p1, p2, p3, p4);
        AddFace(p1, p2, p4, p3);
        AddFace(p1, p3, p4, p2);
        AddFace(p2, p3, p4, p1);

        for (int i = 4; i < points.size(); ++i) {
            if (processed.find(points[i]) != processed.end()) continue;
            processed.insert(points[i]);
            AddPointToHull(points[i]);
            RemoveFaces();
        }
    }

    void AddPointToHull(Point p) {
        bool vis = false;
        for (auto iter = hull.begin(); iter != hull.end(); ++iter) {
            iter->visible = Face::TetrahedronVolume(*iter, p) < 0;
            if (iter->visible) {
                vis = true;
            }
        }

        if (!vis) return;

        for (auto iter = edges.begin(); iter != edges.end(); ++iter) {
            if (iter->CountFaces() != 2) continue;

            if (iter->face1->visible && iter->face2->visible) {
                iter->remove = true;
            } else if (iter->face1->visible && !iter->face2->visible) {
                Point other_p = get_other_point_of_face(*iter->face1, *iter);
                iter->RemoveFace(iter->face1);
                AddFace(iter->p0, iter->p1, p, other_p);
            } else if (!iter->face1->visible && iter->face2->visible) {
                Point other_p = get_other_point_of_face(*iter->face2, *iter);
                iter->RemoveFace(iter->face2);
                AddFace(iter->p0, iter->p1, p, other_p);
            }
        }
    }

    void RemoveFaces() {
        auto it = edges.begin();
        while (it != edges.end()) {
            if (it->remove) {
                size_t key = it->GetKey();
                edge_to_faces.erase(key);
                edges.erase(it++);
            } else it++;
        }

        auto it2 = hull.begin();
        while (it2 != hull.end()) {
            if (it2->visible) {
                hull.erase(it2++);
            } else {
                it2++;
            }
        }
    }

    void AddFace(const Point& a, const Point& b, const Point& c, const Point& p) {
        Face new_face(a, b, c);

        if (Face::TetrahedronVolume(new_face, p) < 0) {
            new_face = Face(b, a, c);
        }

        Edge e1(new_face.vtx[0], new_face.vtx[1]);
        Edge e2(new_face.vtx[1], new_face.vtx[2]);
        Edge e3(new_face.vtx[2], new_face.vtx[0]);

        size_t key1 = e1.GetKey();
        size_t key2 = e2.GetKey();
        size_t key3 = e3.GetKey();

        hull.push_back(new_face);

        if (edge_to_faces.find(key1) == edge_to_faces.end()) {
            edges.push_back(e1);
            edge_to_faces[key1] = &edges.back();
        }

        if (edge_to_faces.find(key2) == edge_to_faces.end()) {
            edges.push_back(e2);
            edge_to_faces[key2] = &edges.back();
        }

        if (edge_to_faces.find(key3) == edge_to_faces.end()) {
            edges.push_back(e3);
            edge_to_faces[key3] = &edges.back();
        }

        edge_to_faces[key1]->AddFace(&hull.back());
        edge_to_faces[key2]->AddFace(&hull.back());
        edge_to_faces[key3]->AddFace(&hull.back());
    }

    Point get_other_point_of_face(const Face& f, const Edge& e) {
        if (f.vtx[0] != e.p0 && f.vtx[0] != e.p1) {
            return f.vtx[0];
        } else if (f.vtx[1] != e.p0 && f.vtx[1] != e.p1) {
            return f.vtx[1];
        } else return f.vtx[2];
    }
};

int main() {
    int number_of_points = 0;
    std::cin >> number_of_points;

    std::vector<Point> points(number_of_points);
    for (int i = 0; i < number_of_points; ++i) {
        std::cin >> points[i].x >> points[i].y >> points[i].z;
    }

    int query_number = 0;
    std::cin >> query_number;

    std::vector<Point> worms;
    for (int i = 0; i < query_number; ++i) {
        Point a;
        std::cin >> a.x >> a.y >> a.z;
    }

    Solution sol(std::move(points));
    double length = sol.Solve(query_number, std::move(worms));

    std::cout << std::fixed << std::setprecision(10) << length << std::endl;

    return 0;
}