#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

class Trie {
  public:
    const int kRootVertex = 0;

    Trie() {
        trie_vertices.push_back(TrieVertex());
        current_vtx = kRootVertex;
    }

    Trie(const std::vector<std::string>& strings) : Trie() {
        for (int i = 0; i < strings.size(); ++i) {
            AddString(strings[i], i);
        }
    }

    // Add string with index=patternNumber in trie.
    void AddString(std::string_view str, int pattern_number) {
        int current_vtx = kRootVertex;

        for (size_t i = 0; i < str.size(); ++i) {
            char current_sym = str[i];

            // add new vtx to trie, if necessary
            if (trie_vertices[current_vtx].go.find(current_sym) == trie_vertices[current_vtx].go.end()) {
                TrieVertex new_trie_vertex = TrieVertex();

                new_trie_vertex.parent_vertex = current_vtx;
                new_trie_vertex.parent_character = current_sym;

                trie_vertices.push_back(new_trie_vertex);

                trie_vertices[current_vtx].go[current_sym] = trie_vertices.size() - 1;
            }

            current_vtx = trie_vertices[current_vtx].go[current_sym];
        }

        trie_vertices[current_vtx].is_leaf = true;
        trie_vertices[current_vtx].leaf_pattern_number.push_back(pattern_number);
    }

    bool IsCurrentVtxLeaf() {
        return trie_vertices[current_vtx].is_leaf;
    }

    const std::vector<int>& VtxGetLeafPatternNumber(int vtx) {
        return trie_vertices[vtx].leaf_pattern_number;
    }

    std::vector<int> VtxGetAllCompressedSuffLink(int vtx) {
        std::vector<int> all_compressed_suff_links;

        vtx = GetCompressedSuffLink(vtx);
        while (vtx != kRootVertex) {
            all_compressed_suff_links.push_back(vtx);
            vtx = GetCompressedSuffLink(vtx);
        }

        return all_compressed_suff_links;
    }

    void ChangeState(char sym, int template_ind, size_t size,
        std::vector<int>& count_starting_subtemplates,
        const std::vector<int>& templates_last_template_pos,
        const std::vector<std::string>& templates) {
        current_vtx = GetLink(current_vtx, sym);

        if (IsCurrentVtxLeaf()) {
            IncreaseNumberOfTemplates(GetCurrentTrieVtx(), template_ind, size,
                count_starting_subtemplates, templates_last_template_pos,
                templates);
        }

        IncreaseForCompressedSuffLinks(template_ind, size,
            count_starting_subtemplates, templates_last_template_pos,
            templates);
    }

    int GetCurrentTrieVtx() {
        return current_vtx;
    }

    void IncreaseNumberOfTemplates(int vtx, int input_ind, size_t input_size,
        std::vector<int>& count_starting_subtemplates,
        const std::vector<int>& templates_last_template_pos,
        const std::vector<std::string>& templates) {
        
        for (auto j : VtxGetLeafPatternNumber(vtx)) {
            int ind = input_ind - templates_last_template_pos[j] - templates[j].size() + 1;
            if (ind >= 0 && ind < input_size)
                count_starting_subtemplates[ind]++;
        }
    }

    void IncreaseForCompressedSuffLinks(int input_ind, size_t input_size,
        std::vector<int>& count_starting_subtemplates,
        const std::vector<int>& templates_last_template_pos,
        const std::vector<std::string>& templates) {
        for (auto go_vtx : VtxGetAllCompressedSuffLink(GetCurrentTrieVtx())) {
            IncreaseNumberOfTemplates(go_vtx, input_ind, input_size,
                count_starting_subtemplates, templates_last_template_pos,
                templates);
        }
    }
  private:
    struct TrieVertex {
        std::unordered_map<char, int> go;
        std::vector<int> leaf_pattern_number;

        int parent_vertex = 0;
        int suff_link = -1;
        int compressed_suff_link = -1;

        char parent_character;
        bool is_leaf = false;
    };

    std::vector<TrieVertex> trie_vertices;
    int current_vtx;

    // Get suffix link for vtx.
    int GetSuffLink(int vtx) {
        if (trie_vertices[vtx].suff_link == -1) {
            if (vtx == 0 || trie_vertices[vtx].parent_vertex == 0) {
                trie_vertices[vtx].suff_link = 0;
            } else {
                trie_vertices[vtx].suff_link = GetLink(GetSuffLink(trie_vertices[vtx].parent_vertex), trie_vertices[vtx].parent_character);
            }
        }

        return trie_vertices[vtx].suff_link;
    }


    // Get correct transition from vtx by sym.
    int GetLink(int vtx, char sym) {
        if (trie_vertices[vtx].go.find(sym) == trie_vertices[vtx].go.end()) {
            if (vtx == 0) {
                trie_vertices[vtx].go[sym] = 0;
            } else {
                trie_vertices[vtx].go[sym] = GetLink(GetSuffLink(vtx), sym);
            }
        }

        return trie_vertices[vtx].go[sym];
    }

    // Compressed suffix links, all are terminal.
    int GetCompressedSuffLink(int vtx) {
        if (trie_vertices[vtx].compressed_suff_link == -1) {
            if (trie_vertices[GetSuffLink(vtx)].is_leaf) {
                trie_vertices[vtx].compressed_suff_link = GetSuffLink(vtx);
            } else if (GetSuffLink(vtx) == 0) {
                trie_vertices[vtx].compressed_suff_link = 0;
            } else {
                trie_vertices[vtx].compressed_suff_link = GetCompressedSuffLink(GetSuffLink(vtx));
            }
        }

        return trie_vertices[vtx].compressed_suff_link;
    }
};

class Solution {
  public:
    Solution(char sep, std::string_view template_with_masks) {
        separator_ = sep;
        mask_size_ = template_with_masks.size();

        std::string sub_template;
        int last_template_pos = 0;
        // Get substring-templates separated by separator_.
        for (int i = 0; i < template_with_masks.size(); ++i) {
            if (template_with_masks[i] == sep) {
                if (sub_template.size() > 0) {
                    templates_last_template_pos.push_back(last_template_pos);
                    templates.push_back(sub_template);
                }

                last_template_pos = i + 1;
                sub_template.clear();
                continue;
            } else {
                sub_template += template_with_masks[i];
            }
        }

        if (sub_template != std::string(1, separator_) && sub_template.size() > 0) {
            templates.push_back(sub_template);
            templates_last_template_pos.push_back(last_template_pos);
        }

        for (int i = template_with_masks.size() - 1; i >= 0; i--) {
            if (template_with_masks[i] == separator_) count_end_masks_++;
            else break;
        }
    }

    std::vector<int> GetStartingPositions(std::string_view input) {
        std::vector<int> starting_positions;

        if (IsEmptyMask(input)) {
            for (int i = 0; i <= input.size() - mask_size_; ++i) {
                starting_positions.push_back(i);
            }

            return starting_positions;
        } else if (SmallInputSize(input)) {
            return starting_positions;
        }

        // Build trie
        Trie trie = Trie(templates);

        std::vector<int> count_starting_subtemplates = CountStartingSubtemplates(input, std::move(trie));

        for (int i = 0; i < input.size(); ++i) {
            if (count_starting_subtemplates[i] == templates.size()) {
                starting_positions.push_back(i);
            }
        }

        return starting_positions;
    }
  private:
    char separator_;
    std::vector<std::string> templates;
    std::vector<int> templates_last_template_pos;
    int count_end_masks_ = 0;
    int mask_size_ = 0;

    bool SmallInputSize(std::string_view input) {
        return (input.size() < mask_size_);
    }

    bool IsEmptyMask(std::string_view input) {
        return (templates.size() == 0);
    }

    std::vector<int> CountStartingSubtemplates(std::string_view input, Trie&& trie) {
        std::vector<int> count_starting_subtemplates(input.size(), 0);

        for (int i = 0; i < input.size() - count_end_masks_; ++i) {
            char sym = input[i];
            trie.ChangeState(sym, i, input.size(), count_starting_subtemplates,
                             templates_last_template_pos, templates);
        }

        return count_starting_subtemplates;
    }
};

int main() {
    std::string template_with_masks;
    std::string input;

    std::cin >> template_with_masks;
    std::cin >> input;

    Solution solution('?', template_with_masks);
    for (auto pos : solution.GetStartingPositions(input)) {
        std::cout << pos << " ";
    }
    std::cout << std::endl;

    return 0;
}