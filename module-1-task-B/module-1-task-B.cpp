#include <iostream>
#include <string_view>
#include <vector>

class PalindromeCounter {
  public:
    PalindromeCounter(std::string_view input) {
        this->input = input;

        this->palindromes_by_parity.resize(2);
        this->palindromes_by_parity[0].resize(input.size(), 0);
        this->palindromes_by_parity[1].resize(input.size(), 1);
    }

    // Use Manacher algorithm to count all palindromes.
    size_t CountPalindromes() {
        CountPalindromesByParity(0);
        CountPalindromesByParity(1);

        long long amount_of_palindromes = 0;
        for (int i = 0; i < input.size(); ++i) {
            amount_of_palindromes += palindromes_by_parity[0][i] + palindromes_by_parity[1][i];
        }

        // Don't count palindromes with length = 1.
        amount_of_palindromes -= input.size();

        return amount_of_palindromes;
    }

  private:
    std::string_view input;
    std::vector<std::vector<int>> palindromes_by_parity;

    // Count all palindromes with the same parity.
    void CountPalindromesByParity(int parity) {
        // Bounds of palindrome with rightmost border
        int rightmost_p_start = 0;
        int rightmost_p_end = -1;

        for (int i = 0; i < input.size(); ++i) {
            if (i <= rightmost_p_end) {
                palindromes_by_parity[parity][i] = std::min(rightmost_p_end - i + 1, palindromes_by_parity[parity][rightmost_p_start + rightmost_p_end - i + (parity + 1) % 2]);
            }

            while (i + palindromes_by_parity[parity][i] < input.size() && i - palindromes_by_parity[parity][i] - (parity + 1) % 2 >= 0
                && input[i + palindromes_by_parity[parity][i]] == input[i - palindromes_by_parity[parity][i] - (parity + 1) % 2]) {
                palindromes_by_parity[parity][i]++;
            }

            if (palindromes_by_parity[parity][i] + i - 1 > rightmost_p_end) {
                rightmost_p_start = i - palindromes_by_parity[parity][i] + parity;
                rightmost_p_end = i + palindromes_by_parity[parity][i] - 1;
            }
        }
    }
};

int main() {
    std::string input;
    std::cin >> input;

    PalindromeCounter solver = PalindromeCounter(input);

    std::cout << solver.CountPalindromes() << std::endl;

    return 0;
}